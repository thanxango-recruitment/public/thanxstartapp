import React from "react";
import { Button, StyleSheet, Text, TextInput, View } from "react-native";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "Start Searching"
    };

    this.onPressSearch = this.onPressSearch.bind(this);
    this.onPressScan = this.onPressScan.bind(this);
  }

  onPressSearch() {
    // ...
  }

  onPressScan() {
    // ...
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome to the Thanxngo Demo App!</Text>
        <View style={styles.input}>
          <TextInput
            value={this.state.text}
            onChangeText={text => this.setState({ text })}
          />
        </View>
        <Button
          onPress={this.onPressSearch}
          title="Search"
          style={styles.button}
        />
        <Button
          onPress={this.onPressScan}
          title="Scan a QRCode"
          style={styles.button}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    alignSelf: "stretch",
    margin: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: "#555",
    color: "#555"
  },
  button: {}
});
